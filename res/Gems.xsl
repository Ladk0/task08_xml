<?xml version="1.0"?>

<xsl:stylesheet version="1.0" xmlns:xls="http://www.w3.org/1999/XSL/Transform" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <body>
                <h2>Gems</h2>
                <table border="1">
                    <tr bgcolor="#00ffff">
                        <th>Name</th>
                        <th>Preciousness</th>
                        <th>Origin</th>
                        <th>Color</th>
                        <th>Transparency</th>
                        <th>Value</th>
                    </tr>
                    <xsl:for-each select="gems/gem">
                        <xls:sort select="@name"/>
                        <tr>
                            <td><xsl:value-of select="@name"/></td>
                            <td><xsl:value-of select="preciousness"/></td>
                            <td><xsl:value-of select="origin"/></td>
                            <td><xsl:value-of select="parameters/color"/></td>
                            <td><xsl:value-of select="parameters/transparency"/></td>
                            <td><xsl:value-of select="value"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>