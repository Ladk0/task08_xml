package Gems;

public class Gem {
    private String name;
    private String preciousness;
    private String origin;
    private VisualParameters visualParameters = new VisualParameters();
    private float value;

    public static class VisualParameters{
        private String color;
        private float transparency;

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public float getTransparency() {
            return transparency;
        }

        public void setTransparency(float transparency) {
            this.transparency = transparency;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPreciousness() {
        return preciousness;
    }

    public void setPreciousness(String preciousness) {
        this.preciousness = preciousness;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return  "name = " + name + '\n' +
                "preciousness = " + preciousness + '\n' +
                "origin = " + origin + '\n' +
                "color = " + visualParameters.color + '\n' +
                "transparency = " + visualParameters.transparency + '%' + '\n' +
                "value = " + value + '\n';
    }
}
