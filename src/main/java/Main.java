import Gems.Gem;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String args[]) throws ParserConfigurationException, IOException, SAXException {
        File file = new File("res/Gems.xml");

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(file);

        NodeList gemNodeList = document.getElementsByTagName("gem");

        List<Gem> gemList = new ArrayList<>();

        for (int i = 0; i < gemNodeList.getLength(); i++) {
            if (gemNodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element gemElement = (Element) gemNodeList.item(i);

                Gem gem = new Gem();
                gem.setName(gemElement.getAttribute("name"));

                NodeList childNodes = gemElement.getChildNodes();
                for (int j = 0; j < childNodes.getLength(); j++) {
                    if (childNodes.item(j).getNodeType() == Node.ELEMENT_NODE) {
                        Element childElement = (Element) childNodes.item(j);

                        switch (childElement.getNodeName()) {
                            case "preciousness":
                                gem.setPreciousness(childElement.getTextContent());
                                break;
                            case "origin":
                                gem.setOrigin(childElement.getTextContent());
                                break;
                            case "value":
                                gem.setValue(Float.valueOf(childElement.getTextContent()));
                                break;
                            case "parameters": {
                                NodeList parametersNodes = childElement.getChildNodes();
                                for (int k = 0; k < parametersNodes.getLength(); k++) {
                                    if (parametersNodes.item(k).getNodeType() == Node.ELEMENT_NODE) {
                                        Element parametersElement = (Element) parametersNodes.item(k);

                                        switch (parametersElement.getNodeName()) {
                                            case "color":
                                                gem.getVisualParameters().setColor(parametersElement.getTextContent());
                                                break;
                                            case "transparency":
                                                gem.getVisualParameters().setTransparency(Float.valueOf(parametersElement.getTextContent()));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                gemList.add(gem);
            }
        }
        for (Gem gem : gemList)
            System.out.println(gem.toString());
    }
}